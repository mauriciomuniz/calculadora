package br.com.calculadora;

import br.com.calculadora.controllers.CalculadoraController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculadoraApplication {

	//private CalculadoraController calculadoraController;

	public static void main(String[] args) {
		SpringApplication.run(CalculadoraApplication.class, args);
	}

}
