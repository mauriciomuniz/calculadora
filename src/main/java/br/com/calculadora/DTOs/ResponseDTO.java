package br.com.calculadora.DTOs;

public class ResponseDTO {

    private Integer resultado;

    public ResponseDTO (){
    }

    public ResponseDTO(Integer resultado) {
        this.resultado = resultado;
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }
}
