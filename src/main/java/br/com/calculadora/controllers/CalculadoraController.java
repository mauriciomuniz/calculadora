package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.ResponseDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public ResponseDTO somar(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() <= 1){

            for (int numero: calculadora.getNumeros()) {
                if (numero % 1 != 0) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aceitamos apenas números naturais");
                }
            }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "São necessários pelo menos dois numeros");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public ResponseDTO subtrair(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() <= 1){
            for (int numero: calculadora.getNumeros()) {
                if (numero % 1 != 0) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aceitamos apenas números naturais");
                }
            }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "São necessários pelo menos dois numeros");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public ResponseDTO multiplicar(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() <= 1){
            for (int numero: calculadora.getNumeros()) {
                if (numero % 1 != 0) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aceitamos apenas números naturais");
                }
            }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "São necessários pelo menos dois numeros");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public ResponseDTO dividir(@RequestBody Calculadora calculadora) {

        if (calculadora.getNumeros().size() != 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário apenas 2 números");
        } else if (calculadora.getNumeros().get(1) == 0 ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não podemos dividir por zero");
        } else if (calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não iremos dividir um número menor por outro maior");
        }
        for (int numero: calculadora.getNumeros()) {
            if (numero % 1 != 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aceitamos apenas números naturais");
            }
        }
        return calculadoraService.dividir(calculadora);
    }
}
