package br.com.calculadora.services;

import br.com.calculadora.DTOs.ResponseDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public ResponseDTO somar(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()) {
            resultado += numero;
        }
        ResponseDTO responseDTO = new ResponseDTO(resultado);
        return responseDTO;
    }

    public ResponseDTO subtrair(Calculadora calculadora) {
        int resultado = calculadora.getNumeros().get(0);
        calculadora.getNumeros().remove(0);

        for (int numero: calculadora.getNumeros()) {
            resultado-=numero;
        }

        ResponseDTO responseDTO = new ResponseDTO(resultado);
        return responseDTO;
    }

    public ResponseDTO multiplicar(Calculadora calculadora) {
        int resultado = 1;
        for (Integer numero : calculadora.getNumeros()) {
            resultado *= numero;
        }
        ResponseDTO responseDTO = new ResponseDTO(resultado);
        return responseDTO;
    }

    public ResponseDTO dividir(Calculadora calculadora) {

        int resultado = calculadora.getNumeros().get(0) / calculadora.getNumeros().get(1);

        ResponseDTO responseDTO = new ResponseDTO(resultado);
        return responseDTO;
    }
}
